#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <algorithm> // sort

#include "LidarFilter.hpp"

// Reads the input and sets the length N.
void LidarFilter::read_length (string s) {
    int length = 1;
    int string_size = sizeof(s)/sizeof(s[0]);
    assert (string_size >= 0);
    for (int i = 0; i < string_size; ++i) {
        if (s[i] == ',') {
            ++length;
        }
    }
    n = length;
}

// Reads the input into the scan array of distances
void LidarFilter::read_scan(string s, float (&scan)[]) {
    // string index
    size_t s_index = 0;
    // input index
    int i_index = 0;
    string delimiter = ", ";
    string token;
    float distance;
    // Delete the brackets "[" and "]"
    s = s.substr(1, s.size() - 3);
    // reads in all the distances
    while ((s_index = s.find(delimiter)) != string::npos) {
        token = s.substr(0, s_index);
        distance = stof(token);
        assert (distance >= 0.0);
        scan[i_index] = distance;
        ++i_index;
        s.erase(0, s_index + 2);
    }
    // reads in last distance where there are no delimiters left
    distance = stof(s);
    assert (distance >= 0);
    scan[i_index] = distance;
}

// Replaces values that are not in bounds within the given range of distances
// with the corresponding min/max value from the scan and updates that filter
void RangeFilter::update(float (&scan)[], float (&filter)[]) {
    float min = 50.01;
    float max = 0.02;
    // finds the min and max value that's within range
    for (int i = 0; i < n; ++i) {
        if (scan[i] < min && scan[i] >= range_min) {
            min = scan[i];
        }
        if (scan[i] > max && scan[i] <= range_max) {
            max = scan[i];
        }
    }
    // fills in the values for the filter
    for (int j = 0; j < n; ++j) {
        if (scan[j] < range_min) {
            filter[j] = min;
        } else if (scan[j] > range_max) {
            filter[j] = max;
        } else {
            filter[j] = scan[j];
        }
    }
}

// Initializes the number of previous scans to store
TMFilter::TMFilter(int num_scans) {
    assert (num_scans > 0);
    d = num_scans;
}

// Returns the median of the current and d previous scans through the referenced
// variable filter
void TMFilter::update(float (&scan)[], float **(&prev_scans), float (&filter)[]) {
    float median;
    int index;
    int bound = d;
    // bound will be the number of previous scans, d
    // or all the scans if we have less than d.
    if (time < d) {
        bound = time;
    }
    for (int i = 0; i < n; ++i) {
        // list of distances to find the median
        float distances[bound + 1];
        for (int j = 0; j < bound; ++j) {
            distances[j] = prev_scans[i][j];
        }
        distances[bound] = scan[i];
        sort(distances, distances + bound + 1);
        index = bound / 2;
        assert (index >= 0);
        // If there is an even number of distances, divide the middle two
        if ((bound + 1) % 2 == 0) {
            median = (distances[index] + distances[index + 1]) / 2.0;
        } else {
            median = distances[index];
        }
        filter[i] = median;
    }
}

// Updates the previous stored distances
void TMFilter::update_prevs(float (&scan)[], float **(&prev_scans)) {
    for (int i = 0; i < n; ++i) {
        // Keep adding distances if d is not reached yet
        if (time < d) {
            prev_scans[i][time] = scan[i];
        } else {
            // Push prev distances back
            for (int j = 0; j < d - 1; ++j) {
                prev_scans[i][j] = prev_scans[i][j + 1];
            }
            // Push most recent scan
            prev_scans[i][d - 1] = scan[i];
        }
    }
}

// Prints out the time, input scan, and filtered output
void LidarFilter::print_filter(ostream& sout, float (&scan)[], float (&filter)[]) {
    sout << time << " [" << scan[0];
    for (int i = 1; i < n; ++i) {
        sout << ", ";
        sout << scan[i];
    }
    sout << "]" << " [" << filter[0];
    for (int j = 1; j < n; ++j) {
        sout << ", ";
        sout << filter[j];
    }
    sout << "]\n";
}

// Reads, updates, and prints the range filter
void RangeFilter::solve (istream& sin, ostream& sout) {
    string s;
    while(getline(sin, s)) {
        if (n < 0) {
            read_length(s);
        }
        float scan[n] = {};
        float filter[n] = {};
        read_scan(s, scan);
        update(scan, filter);
        print_filter(sout, scan, filter);
        ++time;
    }
}

// Reads, updates, and prints the temporal median filter
void TMFilter::solve (istream& sin, ostream& sout) {
    string s;
    // cache of previous distances
    float **prev_scans;
    while(getline(sin, s)) {
        if (n < 0) {
            read_length(s);
            assert (n > -1);
            prev_scans = new float *[n];
            for (int i = 0; i < n; ++i) {
                prev_scans[i] = new float[d];
            }
        }
        float scan[n] = {};
        float filter[n] = {};
        read_scan(s, scan);
        update(scan, prev_scans, filter);
        update_prevs(scan, prev_scans);
        print_filter(sout, scan, filter);
        ++time;
    }
}