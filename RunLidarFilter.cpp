#include <iostream> // cin, cout

#include "LidarFilter.hpp"

using namespace std;

int main () {
    string s;
    // Assumes that the input file's
    // first line contains the number
    // of previous scans to store.
    // If d is 0, then a range filter
    // will be used.
    getline(cin, s);
    int d = stoi(s);
    if (d == 0) {
        RangeFilter rf;
        rf.solve(cin, cout);
    } else {
        TMFilter tmf(d);
        tmf.solve(cin, cout);
    }
    return 0;
}